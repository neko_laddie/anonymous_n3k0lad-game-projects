Project ini akan berisi Game Project buatan Anonymous_N3k0Lad yang sudah selesai, ukuran file dapat bervariasi. Semoga aja penyimpanan GitLab itu tidak terbatas. Soalnya, aku tidak dapat mengandalkan Google Drive atau layanan yang serupa. Semuanya dibatasin :(, menyebalkan sekali.

Kemungkinan, Game disini mengandung banyak sekali copyright, pastikan kalian tidak menggunakannya secara sembarangan.

Credits :
- Author                        : Anonymous_N3k0Lad (biasanya)
- Character Design              : Anonymous_N3k0Lad (biasanya)
- Tileset Design                : [Bisa siapa saja]
- Modified Misc Design          : [Bisa siapa saja] 
- Script Programming            : [Bisa siapa saja]
- Modified Script Programming   : Anonymous_N3k0Lad (biasanya)
- Audio                         : [Bisa siapa saja]
